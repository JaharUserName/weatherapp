package dao;

/**
 * Temperature Value Object
 */
public class TemperatureVo {

    /*Day Temperature*/
    private Double dayTemperature;
    /*Min Temperature*/
    private Double minTemperature;
    /*Max Temperature*/
    private Double maxTemperature;
    /*Night Temperature*/
    private Double nightTemperature;

    public TemperatureVo() {
    }

    /**
     * @param dayTemperature
     * @param nightTemperature
     */
    public TemperatureVo(Double dayTemperature, Double nightTemperature) {
        this.dayTemperature = dayTemperature;
        this.nightTemperature = nightTemperature;
    }

    public Double getDayTemperature() {
        return dayTemperature;
    }

    public void setDayTemperature(Double dayTemperature) {
        this.dayTemperature = dayTemperature;
    }

    public Double getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(Double minTemperature) {
        this.minTemperature = minTemperature;
    }

    public Double getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(Double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public Double getNightTemperature() {
        return nightTemperature;
    }

    public void setNightTemperature(Double nightTemperature) {
        this.nightTemperature = nightTemperature;
    }
}
