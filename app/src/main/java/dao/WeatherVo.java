package dao;

/**
 * Temperature Value Object
 */
public class WeatherVo {

    /*Weather Id*/
    private int id;
    /* Sky Forecast*/
    private String sky;
    /*Weather Description*/
    private String description;
    /*Weather Icon*/
    private String icon;


    public WeatherVo() {
    }

    /**
     * @param id
     * @param description
     * @param sky
     * @param icon
     */
    public WeatherVo(int id, String sky, String description, String icon) {
        this.id = id;
        this.sky = sky;
        this.description = description;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSky() {
        return sky;
    }

    public void setSky(String sky) {
        this.sky = sky;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
