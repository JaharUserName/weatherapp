package weather_managers;

import android.app.Activity;
import android.support.percent.PercentRelativeLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import dao.TemperatureVo;
import dao.WeatherVo;
import hackathon.com.weatherapp.R;
import helpers.KeyValue;

/**
 * CurrentWeatherManager is reponsible for
 * - displaying the weather of location for today
 */
public class CurrentWeatherManager extends WeatherManager {

    public String locationName;
    public ProgressBar progressBar;

    public TextView locationTextView, temperatureTextView,
            descriptionTextView, dayTextView, loadingTextView;
    public ImageView weatherIconImageView, tomorrowBtn, weekBtn;

    /**
     * @param locationName
     * @param activity
     */
    public CurrentWeatherManager(String locationName, Activity activity) {
        this.locationName = locationName;
        this.activity = activity;
        initUIElements();
    }

    @Override
    public void buildQuery() {
        queryBuilder.addCategoryParam(KeyValue.WEATHER);
        queryBuilder.addLocationParam(locationName);
        queryBuilder.addApiKeyParam();
    }

    @Override
    public void onPreUIExecution(Activity activity) {
        progressBar = (ProgressBar) activity.findViewById(R.id.progress_bar_today);
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Makes changes in UI, after the API Response is retrieved
     */
    @Override
    public void onPostUIExecution(final Activity activity) {
        setUIElementsValues();
        progressBar.setVisibility(View.GONE);
        loadingTextView.setVisibility(View.GONE);
    }


    @Override
    public List<WeatherVo> getWeatherVOs() {
        try {
            JSONObject listJsonObject = new JSONObject(apiResponse);
            JSONArray weatherJsonArray = (JSONArray) listJsonObject.get(KeyValue.WEATHER);
            JSONObject weatherJsonObject = new JSONObject(weatherJsonArray.toString().replace("[", "").replace("]", ""));

            Integer weatherId = Integer.parseInt(weatherJsonObject.get(KeyValue.WEATHER_ID).toString());
            String weatherSky = (String) weatherJsonObject.get(KeyValue.WEATHER_MAIN);
            String weatherDescription = (String) weatherJsonObject.get(KeyValue.WEATHER_DESCRIPTION);
            String weatherIcon = (String) weatherJsonObject.get(KeyValue.WEATHER_ICON);

            WeatherVo weatherVo = new WeatherVo(weatherId, weatherSky, weatherDescription, weatherIcon);
            weatherVos.add(weatherVo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return weatherVos;
    }

    @Override
    public List<TemperatureVo> getTemperatureVOs() {
        try {
            JSONObject listJsonObject = new JSONObject(apiResponse);
            JSONObject jsonObject = (JSONObject) listJsonObject.get(KeyValue.TEMP_MAIN);

            Double dayTemperature = Double.parseDouble(jsonObject.getString(KeyValue.TEMP));
            Double nightTemperature = Double.parseDouble(jsonObject.getString(KeyValue.TEMP_MIN));

            TemperatureVo temperatureVo = new TemperatureVo(dayTemperature, nightTemperature);
            temperatureVos.add(temperatureVo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temperatureVos;
    }

    /**
     * Initializes UI Elements from XML
     * Sets Listeners to Elements
     */
    public void initUIElements() {
        locationTextView = (TextView) activity.findViewById(R.id.text_location);
        temperatureTextView = (TextView) activity.findViewById(R.id.text_temperature);
        descriptionTextView = (TextView) activity.findViewById(R.id.text_weather_description);
        dayTextView = (TextView) activity.findViewById(R.id.text_day);
        loadingTextView = (TextView) activity.findViewById(R.id.text_loading);
        weatherIconImageView = (ImageView) activity.findViewById(R.id.image_weather);
        tomorrowBtn = (ImageView) activity.findViewById(R.id.btn_up);
        weekBtn = (ImageView) activity.findViewById(R.id.btn_below);

        tomorrowBtn.setImageResource(R.drawable.btn_tomorrow);
        weekBtn.setImageResource(R.drawable.btn_next_week);

        weekBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FutureWeatherManager weatherManager = new FutureWeatherManager(locationName, 7, activity);
                weatherManager.getData();
            }
        });

        tomorrowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FutureWeatherManager weatherManager = new FutureWeatherManager(locationName, 1, activity);
                weatherManager.getData();
            }
        });

    }

    /**
     * Sets values to UI Elements
     */
    public void setUIElementsValues() {
        TemperatureVo temperatureVo = getTemperatureVOs().get(0);
        WeatherVo weatherVo = getWeatherVOs().get(0);

        dayTextView.setText(KeyValue.TODAY);
        locationTextView.setText(locationName);
        temperatureTextView.setText(getTempCelsiusString(temperatureVo.getDayTemperature()));
        descriptionTextView.setText(weatherVo.getDescription());
        weatherIconImageView.setImageResource(getIconResource(weatherVo.getIcon()));


        PercentRelativeLayout percentRelativeLayout = (PercentRelativeLayout) activity.findViewById(R.id.pl_layout);
        percentRelativeLayout.setVisibility(View.GONE);
        RelativeLayout weatherBulkRelativeLayout = (RelativeLayout) activity.findViewById(R.id.rl_weather_bulk);
        weatherBulkRelativeLayout.setVisibility(View.VISIBLE);
    }


}
