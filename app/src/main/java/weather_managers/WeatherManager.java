package weather_managers;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import dao.TemperatureVo;
import dao.WeatherVo;
import helpers.KeyValue;
import helpers.QueryBuilder;

/**
 * WeatherManager is responsible for
 * -  retrieving the data from REST-API Service
 * -  converting the Api Response to JSON Objects
 * -  converting JSON Objects to Value Objects(WeatherVo, TemperatureVo)
 */
public class WeatherManager {

    /*Query builder for building Query*/
    public QueryBuilder queryBuilder = new QueryBuilder();
    /*Api Response from Server Side*/
    public String apiResponse = new String();
    /*List of Weather Value Objects*/
    public ArrayList<WeatherVo> weatherVos = new ArrayList<>();
    /*List of Temperature Value Objects*/
    public ArrayList<TemperatureVo> temperatureVos = new ArrayList<>();

    public Activity activity;

    /*Builds necessary query depending on Manager*/
    public void buildQuery() {
    }

    /*Starts Calling the API Response from Server*/
    public void getData() {
        new RetrieveWeatherTask().execute();
    }

    public void setApiResponse(String apiResponse) {
        this.apiResponse = apiResponse;
    }


    /*Task, the goal of which is to get API Response from Server*/
    public class RetrieveWeatherTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            onPreUIExecution(activity);
        }

        protected String doInBackground(String... urls) {
            buildQuery();
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(queryBuilder.getQuery());

            HttpResponse response = null;
            try {
                response = httpclient.execute(httpget);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(activity, KeyValue.PLEASE_CHECK_CONNECTION, Toast.LENGTH_SHORT).show();
            }

            if (response.getStatusLine().getStatusCode() == 200) {
                try {
                    apiResponse = EntityUtils.toString(response.getEntity());
                } catch (IOException e) {
                    Toast.makeText(activity, KeyValue.PLEASE_CHECK_CONNECTION, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                setApiResponse(apiResponse);

                Log.i("Api Response", "Success to get server response");
            } else {
                Log.i("Api Response", "Failed to get server response");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            getWeatherVOs();
            getTemperatureVOs();
            onPostUIExecution(activity);
        }
    }

    /**
     * Makes changes in UI, before the API Response is retrieved
     */
    public void onPreUIExecution(Activity activity) {
    }

    /**
     * Makes changes in UI, after the API Response is retrieved
     */
    public void onPostUIExecution(Activity activity) {
    }

    /**
     * Converts the Json Objects into weatherVo
     *
     * @return the list of Weather Value Objects
     */
    public List<WeatherVo> getWeatherVOs() {
        return null;
    }

    /**
     * Converts the Json Objects into temperatureVo
     *
     * @return the list of Temperature Value Objects
     */
    public List<TemperatureVo> getTemperatureVOs() {
        return null;
    }

    /**
     * Converts Kelvin degree to Celsius degree
     *
     * @param temperatureKelvin
     * @return temperatureCelcius
     */
    public String getTempCelsiusString(Double temperatureKelvin) {
        Double temperatureCelcius = temperatureKelvin - 273;
        String temperatureCelciusString = temperatureCelcius.intValue() + "°";
        return temperatureCelciusString;
    }

    /**
     * Returns iconResource Integer for iconName
     *
     * @param iconName
     * @return iconResource
     */
    public int getIconResource(String iconName) {
        int iconResourceInt = activity.getResources().getIdentifier("ic_" + iconName, KeyValue.DRAWABLE, activity.getPackageName());
        return iconResourceInt;
    }

    /**
     * return the list of the following 7 days(week)
     */
    public ArrayList<String> getNextWeek() {
        Calendar calendar = Calendar.getInstance();

        String[] days = new String[]{"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        ArrayList<String> nextWeek = new ArrayList<>();
        String[] daysBefore = Arrays.copyOfRange(days, 0, dayOfWeek);
        String[] daysAfter = Arrays.copyOfRange(days, dayOfWeek, days.length);

        for (String dayAfter : daysAfter) {
            nextWeek.add(dayAfter);
        }

        for (String dayBefore : daysBefore) {
            nextWeek.add(dayBefore);
        }

        return nextWeek;
    }


}