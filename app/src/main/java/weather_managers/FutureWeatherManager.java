package weather_managers;

import android.app.Activity;
import android.support.percent.PercentRelativeLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dao.TemperatureVo;
import dao.WeatherVo;
import hackathon.com.weatherapp.R;
import helpers.KeyValue;

/**
 * FutureWeatherManager is responsible for:
 * - displaying the weather of location for tomorrow
 * - displaying the weather of location for next 7 days
 */
public class FutureWeatherManager extends WeatherManager {

    /**
     * UI Elements for Tommorrow
     */
    TextView locationTextView, temperatureTextView,
            descriptionTextView, dayTextView;
    ImageView weatherIconImageView;

    /**
     * UI Elements for NextWeekForecast
     */
    TextView dayOneText, dayTwoText, dayThreeText, dayFourText,
            dayFiveText, daySixText, daySevenText;
    TextView dayOneDegreeText, dayTwoDegreeText, dayThreeDegreeText,
            dayFourDegreeText, dayFiveDegreeText, daySixDegreeText, daySevenDegreeText;
    ImageView dayOneWeatherImage, dayTwoWeatherImage, dayThreeWeatherImage, dayFourWeatherImage,
            dayFiveWeatherImage, daySixWeatherImage, daySevenWeatherImage;

    public String locationName;
    public int numOfDays;
    public ProgressBar progressBar;

    /**
     * @param locationName
     * @param numOfDays
     * @param activity
     */
    public FutureWeatherManager(String locationName, int numOfDays, Activity activity) {
        this.locationName = locationName;
        this.numOfDays = numOfDays;
        this.activity = activity;
        initUIElements(numOfDays);
    }

    /**
     * Initializes the UI Elements from XML
     */
    public void initUIElements(int numOfDays) {
        if (numOfDays == 1) {
            initUIElementsForTomorrowForecast();
        } else if (numOfDays == 7) {
            initUIElementsForNextWeekForecast();
        }
    }

    @Override
    public void buildQuery() {
        queryBuilder.addCategoryParam(KeyValue.FORECAST_DAILY);
        queryBuilder.addLocationParam(locationName);
        queryBuilder.addDaysNumberParam(numOfDays);
        queryBuilder.addApiKeyParam();
    }

    @Override
    public void onPreUIExecution(Activity activity) {
        progressBar = (ProgressBar) activity.findViewById(R.id.progress_bar_today);
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Makes changes in UI, after the API Response is retrieved
     */
    @Override
    public void onPostUIExecution(Activity activity) {
        if (numOfDays == 1) {
            onPostUIExecutionForTomorrow(activity);
        } else if (numOfDays == 7) {
            onPostUIExecutionForNextWeek(activity);
        }
        progressBar.setVisibility(View.GONE);
    }

    /**
     * Makes changes in Tomorrow Forecast UI, after the API Response is retrieved
     */
    public void onPostUIExecutionForTomorrow(Activity activity) {
        setUIElementsValuesForTomorrowForecast();
    }

    /**
     * Makes changes in Next Week Forecast UI, after the API Response is retrieved
     */
    public void onPostUIExecutionForNextWeek(Activity activity) {
        setUIElementsValuesForNextWeekForecast();
    }

    /**
     * Initializes (Finds) the Tomorrow Forecast UI Elements from XML
     */
    public void initUIElementsForTomorrowForecast() {
        locationTextView = (TextView) activity.findViewById(R.id.text_location);
        temperatureTextView = (TextView) activity.findViewById(R.id.text_temperature);
        descriptionTextView = (TextView) activity.findViewById(R.id.text_weather_description);
        dayTextView = (TextView) activity.findViewById(R.id.text_day);
        weatherIconImageView = (ImageView) activity.findViewById(R.id.image_weather);

        ImageView weekBtn = (ImageView) activity.findViewById(R.id.btn_below);
        weekBtn.setImageResource(R.drawable.btn_next_week);
        weekBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FutureWeatherManager weatherManager = new FutureWeatherManager(locationName, 7, activity);
                weatherManager.getData();
            }
        });

        ImageView todayBtn = (ImageView) activity.findViewById(R.id.btn_up);
        todayBtn.setImageResource(R.drawable.btn_today);
        todayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CurrentWeatherManager weatherManager = new CurrentWeatherManager(locationName, activity);
                weatherManager.getData();
            }
        });
    }

    /**
     * Initializes (Finds) the Next Week Forecast UI Elements from XML
     */
    public void initUIElementsForNextWeekForecast() {
        dayTextView = (TextView) activity.findViewById(R.id.text_day);
        dayOneText = (TextView) activity.findViewById(R.id.text_day_one_name);
        dayTwoText = (TextView) activity.findViewById(R.id.text_day_two_name);
        dayThreeText = (TextView) activity.findViewById(R.id.text_day_three_name);
        dayFourText = (TextView) activity.findViewById(R.id.text_day_four_name);
        dayFiveText = (TextView) activity.findViewById(R.id.text_day_five_name);
        daySixText = (TextView) activity.findViewById(R.id.text_day_six_name);
        daySevenText = (TextView) activity.findViewById(R.id.text_day_seven_name);

        dayOneDegreeText = (TextView) activity.findViewById(R.id.text_day_one_degree);
        dayTwoDegreeText = (TextView) activity.findViewById(R.id.text_day_two_degree);
        dayThreeDegreeText = (TextView) activity.findViewById(R.id.text_day_three_degree);
        dayFourDegreeText = (TextView) activity.findViewById(R.id.text_day_four_degree);
        dayFiveDegreeText = (TextView) activity.findViewById(R.id.text_day_five_degree);
        daySixDegreeText = (TextView) activity.findViewById(R.id.text_day_six_degree);
        daySevenDegreeText = (TextView) activity.findViewById(R.id.text_day_seven_degree);

        dayOneWeatherImage = (ImageView) activity.findViewById(R.id.image_day_one_weather);
        dayTwoWeatherImage = (ImageView) activity.findViewById(R.id.image_day_two_weather);
        dayThreeWeatherImage = (ImageView) activity.findViewById(R.id.image_day_three_weather);
        dayFourWeatherImage = (ImageView) activity.findViewById(R.id.image_day_four_weather);
        dayFiveWeatherImage = (ImageView) activity.findViewById(R.id.image_day_five_weather);
        daySixWeatherImage = (ImageView) activity.findViewById(R.id.image_day_six_weather);
        daySevenWeatherImage = (ImageView) activity.findViewById(R.id.image_day_seven_weather);

        ImageView weekBtn = (ImageView) activity.findViewById(R.id.btn_below);
        weekBtn.setImageResource(R.drawable.btn_tomorrow);
        weekBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FutureWeatherManager weatherManager = new FutureWeatherManager(locationName, 1, activity);
                weatherManager.getData();
            }
        });

        ImageView todayBtn = (ImageView) activity.findViewById(R.id.btn_up);
        todayBtn.setImageResource(R.drawable.btn_today);
        todayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CurrentWeatherManager weatherManager = new CurrentWeatherManager(locationName, activity);
                weatherManager.getData();
            }
        });
    }

    /**
     * Sets the values to Tomorrow Forecast UI Elements
     */
    public void setUIElementsValuesForTomorrowForecast() {
        TemperatureVo temperatureVo = getTemperatureVOs().get(0);
        WeatherVo weatherVo = getWeatherVOs().get(0);

        dayTextView.setText(KeyValue.TOMORROW);
        locationTextView.setText(locationName);
        temperatureTextView.setText(getTempCelsiusString(temperatureVo.getDayTemperature()));
        descriptionTextView.setText(weatherVo.getDescription());
        weatherIconImageView.setImageResource(getIconResource(weatherVo.getIcon()));

        PercentRelativeLayout percentRelativeLayout = (PercentRelativeLayout) activity.findViewById(R.id.pl_layout);
        percentRelativeLayout.setVisibility(View.GONE);

        RelativeLayout weatherBulkRelativeLayout = (RelativeLayout) activity.findViewById(R.id.rl_weather_bulk);
        weatherBulkRelativeLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Sets the values to Next Week Forecast UI Elements
     */
    public void setUIElementsValuesForNextWeekForecast() {
        final RelativeLayout weatherBulkRelativeLayout = (RelativeLayout) activity.findViewById(R.id.rl_weather_bulk);
        weatherBulkRelativeLayout.setVisibility(View.GONE);

        ArrayList<WeatherVo> weatherVos = (ArrayList<WeatherVo>) getWeatherVOs();
        ArrayList<TemperatureVo> temperatureVos = (ArrayList<TemperatureVo>) getTemperatureVOs();

        dayOneDegreeText.setText(getTempCelsiusString(temperatureVos.get(0).getDayTemperature()));
        dayTwoDegreeText.setText(getTempCelsiusString(temperatureVos.get(1).getDayTemperature()));
        dayThreeDegreeText.setText(getTempCelsiusString(temperatureVos.get(2).getDayTemperature()));
        dayFourDegreeText.setText(getTempCelsiusString(temperatureVos.get(3).getDayTemperature()));
        dayFiveDegreeText.setText(getTempCelsiusString(temperatureVos.get(4).getDayTemperature()));
        daySixDegreeText.setText(getTempCelsiusString(temperatureVos.get(5).getDayTemperature()));
        daySevenDegreeText.setText(getTempCelsiusString(temperatureVos.get(6).getDayTemperature()));

        dayOneWeatherImage.setImageResource(getIconResource(weatherVos.get(0).getIcon()));
        dayTwoWeatherImage.setImageResource(getIconResource(weatherVos.get(1).getIcon()));
        dayThreeWeatherImage.setImageResource(getIconResource(weatherVos.get(2).getIcon()));
        dayFourWeatherImage.setImageResource(getIconResource(weatherVos.get(3).getIcon()));
        dayFiveWeatherImage.setImageResource(getIconResource(weatherVos.get(4).getIcon()));
        daySixWeatherImage.setImageResource(getIconResource(weatherVos.get(5).getIcon()));
        daySevenWeatherImage.setImageResource(getIconResource(weatherVos.get(6).getIcon()));

        ArrayList<String> nextWeek = getNextWeek();
        dayOneText.setText(nextWeek.get(0));
        dayTwoText.setText(nextWeek.get(1));
        dayThreeText.setText(nextWeek.get(2));
        dayFourText.setText(nextWeek.get(3));
        dayFiveText.setText(nextWeek.get(4));
        daySixText.setText(nextWeek.get(5));
        daySevenText.setText(nextWeek.get(6));

        dayTextView.setText(KeyValue.NEXT_SEVEN_DAYS);
        PercentRelativeLayout percentRelativeLayout = (PercentRelativeLayout) activity.findViewById(R.id.pl_layout);
        percentRelativeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public List<WeatherVo> getWeatherVOs() {
        try {

            JSONObject responseJsonObject = new JSONObject(apiResponse);
            JSONArray listJsonArray = (JSONArray) responseJsonObject.get(KeyValue.LIST);

            for (int i = 0; i < listJsonArray.length(); i++) {
                JSONObject listJsonObject = new JSONObject(listJsonArray.get(i).toString());
                JSONArray weatherJsonArray = (JSONArray) listJsonObject.get(KeyValue.WEATHER);
                JSONObject weatherJsonObject = new JSONObject(weatherJsonArray.toString().replace("[", "").replace("]", ""));

                Integer weatherId = Integer.parseInt(weatherJsonObject.get(KeyValue.WEATHER_ID).toString());
                String weatherSky = (String) weatherJsonObject.get(KeyValue.WEATHER_MAIN);
                String weatherDescription = (String) weatherJsonObject.get(KeyValue.WEATHER_DESCRIPTION);
                String weatherIcon = (String) weatherJsonObject.get(KeyValue.WEATHER_ICON);

                WeatherVo weatherVo = new WeatherVo(weatherId, weatherSky, weatherDescription, weatherIcon);
                weatherVos.add(weatherVo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return weatherVos;
    }

    @Override
    public List<TemperatureVo> getTemperatureVOs() {
        try {
            JSONObject responseJsonObject = new JSONObject(apiResponse);
            JSONArray listJsonArray = (JSONArray) responseJsonObject.get(KeyValue.LIST);

            for (int i = 0; i < listJsonArray.length(); i++) {
                JSONObject listJsonObject = new JSONObject(listJsonArray.get(i).toString());
                JSONObject jsonObject = (JSONObject) listJsonObject.get(KeyValue.TEMP);

                Double dayTemperature = Double.parseDouble(jsonObject.getString(KeyValue.TEMP_DAY));
                Double nightTemperature = Double.parseDouble(jsonObject.getString(KeyValue.TEMP_MIN_2));

                TemperatureVo temperatureVo = new TemperatureVo(dayTemperature, nightTemperature);
                temperatureVos.add(temperatureVo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temperatureVos;
    }
}
