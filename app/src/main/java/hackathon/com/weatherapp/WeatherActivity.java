package hackathon.com.weatherapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import helpers.KeyValue;
import weather_managers.CurrentWeatherManager;

/**
 *
 * */
public class WeatherActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    String initLocation = KeyValue.INIT_LOCATION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_weather);

        PercentRelativeLayout percentRelativeLayout = (PercentRelativeLayout) findViewById(R.id.pl_layout);
        percentRelativeLayout.setVisibility(View.GONE);

        if (isNetworkAvailable()) {
            CurrentWeatherManager weatherManager = new CurrentWeatherManager(initLocation, WeatherActivity.this);
            weatherManager.getData();
        } else {
            Toast.makeText(WeatherActivity.this, KeyValue.PLEASE_CHECK_CONNECTION, Toast.LENGTH_SHORT).show();
        }

        getSupportActionBar().setTitle(R.string.choose_your_location);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        String output = query.substring(0, 1).toUpperCase() + query.substring(1, query.length()).toLowerCase();
        CurrentWeatherManager weatherManager = new CurrentWeatherManager(output, WeatherActivity.this);
        weatherManager.getData();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }

    /**
     * Checks the connection to network
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) WeatherActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
