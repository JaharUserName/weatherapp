package helpers;

/**
 * Class with static values
 */
public class KeyValue {


    public static String INIT_LOCATION = "Tokio";

    public static String REST_BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static String API_KEY = "191040edf9a9956d68130b255be86ffb";

    public static String FORECAST_DAILY = "forecast/daily";
    public static String WEATHER = "weather";

    public static String LIST = "list";

    public static String WEATHER_ID = "id";
    public static String WEATHER_MAIN = "main";
    public static String WEATHER_DESCRIPTION = "description";
    public static String WEATHER_ICON = "icon";

    public static String TEMP = "temp";
    public static String TEMP_DAY = "day";
    public static String TEMP_MIN_2 = "min";
    public static String TEMP_MAIN = "main";
    public static String TEMP_MIN = "temp_min";

    public static String DRAWABLE = "drawable";

    public static String TODAY = "Today";
    public static String TOMORROW = "Tomorrow";
    public static String NEXT_SEVEN_DAYS = "Next 7 days";

    public static String PLEASE_CHECK_CONNECTION = "Please check your internet connection";

}
