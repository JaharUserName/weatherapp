package helpers;

/**
 * Custom Utility class for
 * creating the REST-API URL
 */
public class QueryBuilder {

    /*Initial REST_BASE_URL*/
    private String query = KeyValue.REST_BASE_URL;

    private StringBuilder stringBuilder = new StringBuilder().append(query);

    /**
     * Adds the category
     *
     * @param category f.e : FORECAST_DAILY, WEATHER
     */
    public void addCategoryParam(String category) {
        if (category != null) {
            query = stringBuilder.append(category + "/").toString();
        }
    }

    /**
     * Adds the location
     *
     * @param locationParam f.e : Berlin
     */
    public void addLocationParam(String locationParam) {
        if (locationParam != null) {
            query = stringBuilder.append("?q=" + locationParam + "&").toString();
        }
    }

    /**
     * Adds the number of forecasted days
     *
     * @param numOfDays f.e : 7
     */
    public void addDaysNumberParam(Integer numOfDays) {
        if (numOfDays != null) {
            query = stringBuilder.append("cnt=" + numOfDays + "&").toString();
        }
    }

    /**
     * Adds the API KEY to query
     */
    public void addApiKeyParam() {
        query = stringBuilder.append("appid=" + KeyValue.API_KEY).toString();
    }

    /**
     * @return the ready query
     */
    public String getQuery() {
        return query;
    }

}
